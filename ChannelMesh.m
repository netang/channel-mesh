function [ faces,vertices, markfaces, markvertices] = CableMesh(L1, L2, NL1, NL2, R, NR, n)
%   Функция генерирует сетку для кабеля.
%   Принятые обозначения:
%   L1 - длина первой части кабеля
%   L2 - длина второй части кабеля
%   NL1 - дискретизация (по L1) первой части кабеля
%   NL2 - дискретизация (по L2) второй части кабеля
%   R - радиус кабеля
%   NR - количество окружностей на дисках
%   n - количество точек на окружности кабеля
%   запускать: ChannelMesh(160, 60, 50, 8, 10, 5, 50);


alpha = linspace(0, 2*pi, n+1); %0 degree equals 180 degress -> n+1
alpha = alpha(1:end-1)'; % разбиваем окружность на n частей. (углы, углы, углы)
%строим окружность
x = R*sin(alpha); 
y = R*cos(alpha);
%вытягиваем её по оси Z
z1 = linspace(0, L1, NL1);
z2 = linspace(L1, L1+L2, NL2);
% z = 0:Lh:L;
z = [z1 z2(2:end)];
Zn = length(z); % количество слоев по оси Z;
vertices = zeros(n*Zn, 3); % готовим массив под наши вершины
% n*Zn - вершины поверхности кабеля (без боковых дисков)
% k*Zn - вершины поверхности внутреннего провода кабеля

tmp_z = z(ones(1,n),:); % также можно через repmat(z,k,1);
tmp_z = tmp_z(:);       % вытягиваем в вектор
vertices(1:n*Zn,:) = [ repmat([x y], NL1+(NL2-1), 1) tmp_z];
markvertices = 1*ones(n, 1);
% markvertices = [];
markvertices = [markvertices; zeros( (NL1-2)*n, 1)];
markvertices = [markvertices; 4*ones(n, 1)];
markvertices = [markvertices; zeros( (NL2-2)*n, 1)];
markvertices = [markvertices; 2*ones(n, 1)];



% структура faces такова: боковая поверхность цилиндра -> боковая поверхность
% внутреннего цилиндра -> нижний диск -> верхний диск -> средний диск (порядок можно поменять -- не проблемно)
% (Zn-1)*n*2 - кол-во треугольников на боковой поверхности канала (без дисков)

% обозначаем грани для треугольников боковой поверхности внешнего цилиндра
%--------------------------------------------------------------------------
ind = 1:n-1;
M = repmat(ind, Zn-1, 1);
nv = 0:n:(Zn-2)*n;
A = M + repmat(nv', 1, n-1);   %вершины с номером i
A = A';
B = A + 1;                     %вершины с номером i + 1 
C = (A + n);                   %вершины с номером i + n
D = (C + 1);                   %вершины с номером i + n + 1
faces = [    A(:)      B(:)      C(:);
             C(:)      D(:)      B(:);
             B(end,:)' A(1,:)'   D(end,:)'
             D(end,:)' C(1,:)'   A(1,:)'];
%--------------------------------------------------------------------------
markfaces = zeros((Zn-1)*n*2, 1);


%Генерация нижнего диска и сцепка последнего к кабелю.
%---------------------------------------------------
[tmp_f, v] = Disk(R, n, NR, vertices(1:n,:));
f = tmp_f;
% [f, v] = Disk(R, n, NR, vertices(1:n,:));
vertices = [vertices; v(n+1:end, :)];
markvertices = [markvertices; 1*ones(size(v,1)-n, 1)];
% tic;
indexes = find( f > n );
f(indexes) = f(indexes) + (n*Zn-n);
faces = [faces; f];
% find_time = toc;
markfaces = [markfaces; 1*ones(size(f, 1), 1)];
%----------------------------------------------------


% Генерация верхнего диска и сцепка последнего к кабелю.
%---------------------------------------------------
% [f, v] = Disk(R, n, NR, vertices((Zn-1)*n+1:Zn*n,:));% (Zn-1)*n+1:Zn*n - вершины последнего кольца внешней боковой поверхности цилиндра
f = tmp_f;
v(:, 3) = L1 + L2;
size_vert = size(vertices, 1);
vertices = [vertices; v(n+1:end, :)];
markvertices = [markvertices; 2*ones(size(v,1)-n,1)];
% tic;
indexes = find( f > n );
f(indexes) = f(indexes) + ( size_vert-n );
indexes = find( f <= n );
f(indexes) = f(indexes) + ( (Zn-1)*n );
faces = [faces; f];
% find_time = toc;
markfaces = [markfaces; 2*ones(size(f, 1), 1)];
%----------------------------------------------------


%Генерация центрального диска и сцепка последнего к кабелю.
%---------------------------------------------------
% [f, v] = Disk(R, n, NR, vertices((NL1-1)*n+1:NL1*n,:));% (Zn-1)*n+1:Zn*n - вершины последнего кольца внешней боковой поверхности цилиндра
f = tmp_f;
v(:, 3) = L1;
size_vert = size(vertices, 1);
vertices = [vertices; v(n+1:end, :)];
markvertices = [markvertices; 3*ones(size(v,1)-n, 1)];
% tic;
markfacesSize = size(markfaces, 1);
markfaces = [markfaces; 3*ones(size(f, 1), 1)];
ind1 = find( f(:,2) <= n & f(:,1) <= n );
markfaces( ind1 + markfacesSize ) = 4;
ind2 = find( f(:,2) <= n & f(:,3) <= n );
markfaces( ind2 + markfacesSize ) = 4;
ind3 = find( f(:,3) <= n & f(:,1) <= n ) ;
markfaces( ind3 + markfacesSize ) = 4;
indexes = find( f > n );
f(indexes) = f(indexes) + ( size_vert-n );
indexes = find( f <= n );
f(indexes) = f(indexes) + ( (NL1-1)*n );
faces = [faces; f];
% find_time = toc;

%----------------------------------------------------

% size(vertices)
% size(markvertices)
% size(faces)
% size(markfaces)


RM = [ cos(pi/2)         0             -sin(pi/2) % матрица поворота (Oy)
       0                 1             0
       sin(pi/2)         0           cos(pi/2) ];
   vertices = vertices*RM;
figure();
% trisurf(faces( find(markfaces == 4), : ), vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);
trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);
xlabel('x');
ylabel('y');
zlabel('z');

end

